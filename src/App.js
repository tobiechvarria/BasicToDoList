import React, { Component } from 'react';
import NavBar from './components/navbar';
import ItemList from './components/itemList';

import "./App.css";

class App extends Component {

  render() {
    return (
      <div className="App">
        <NavBar />
        <ItemList />
      </div>
    );
  }
}

export default App;
