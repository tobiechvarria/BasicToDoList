import React from 'react'
import { AppBar, Typography } from '@material-ui/core';
import './navbar.css'
export default () => {
  return (
    <AppBar position="static" color="default">
        <Typography variant="title" color="inherit" className="typography__text">toDoList( props )</Typography>
    </AppBar>
  )
}
