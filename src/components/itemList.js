import React, { Component, Fragment } from 'react'
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import { Typography, Grid, Paper, Input } from '@material-ui/core';

import './itemList.css';

export default class ItemList extends Component {
    state = {
        items: []
      }
    
    addItem = () =>{
        const inputValue = document.getElementById('task').value;
        if(inputValue.length > 0){
          let items = [...this.state.items];
          items.push(inputValue);
          this.setState({items:items});
        }
      }
      removeItem = (id) =>{
        let items = [...this.state.items];
        let trash = (items.length === 1) ? items.splice(0, 1) : items.splice(id, id+1);
        this.setState({items: items});
      }
    render() {
        return (
            <Grid container justify='center'>
                <Paper square={true} className="paper__container">
                    <Paper square={true} elevation={0} className="paper">
                        <Grid container className="grid__header__container">               
                            <Grid item className="grid__header" xs={6}>      
                                <Typography variant="title" noWrap={true} className="grid__header__text">Add Item</Typography>   
                            </Grid>               
                            <Grid item xs={6}>
                                <Button onClick={this.addItem} className="grid__header-btn" mini><AddIcon/></Button>
                            </Grid>
                            <Grid item xs={12} className="grid_item_input">
                                <Input 
                                    id="task" 
                                    label="Task" 
                                    fullWidth={true}
                                    multiline={true}
                                    placeholder="Input Item"
                                    className="grid__header--input" 
                                    />
                            </Grid>
                        </Grid>
                        <Grid container className="grid__item__container">
                        {
                            this.state.items.map((item, index) => {
                                return (
                                <Fragment key={index}>
                                    <Grid item zeroMinWidth xs={8} className="grid__item">
                                        <Typography noWrap className="grid__item--text">{item}</Typography>      
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Button 
                                            className="grid__item--delete" 
                                            key={index} onClick={(e) => this.removeItem(index)}
                                            >
                                            <DeleteIcon/>
                                        </Button>
                                    </Grid>
                                </Fragment>
                                )
                            })
                        }
                        </Grid>
                    </Paper>
                </Paper>
            </Grid>
        )
    }
}
